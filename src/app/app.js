'use strict';

var baseAngularProjectControllers = angular.module("baseAngularProjectControllers", []);
var baseAngularProjectServices    = angular.module("baseAngularProjectServices", ["ngResource"]);
var baseAngularProjectDirectives  = angular.module("baseAngularProjectDirectives", []);
var baseAngularProjectFilters     = angular.module("baseAngularProjectFilters", []);

// Declare app level module which depends on views, and components
var baseAngularProject = angular.module("baseAngularProject", [
    "ngRoute",
    "ngAria",
    "ngAnimate",
    "baseAngularProjectControllers",
    "baseAngularProjectServices",
    "baseAngularProjectDirectives",
    "baseAngularProjectFilters",
    "ngMaterial"
]);

baseAngularProject.config(["$routeProvider", "$httpProvider", "$mdThemingProvider", function ($routeProvider, $httpProvider, $mdThemingProvider) {
    $routeProvider.when("/", {
        templateUrl: "components/home/home.html",
        controller: "homeController"
    })
    .otherwise({redirectTo: "/"});

    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = false;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";

    $mdThemingProvider.theme('default')
        .primaryPalette('blue-grey')
        .accentPalette('indigo')


}]);
