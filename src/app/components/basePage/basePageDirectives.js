//Directive to display the base page
baseAngularProjectDirectives.directive('basePageDisplay', function(){
    return {
        restrict: 'AEC',
        templateUrl: 'components/basePage/basePage.html'
    }
});