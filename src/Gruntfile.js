'use strict';
module.exports = function(grunt){
	grunt.initConfig({
        project: {
            app: ['app'],
            assets: ['<%= project.app %>/assets'],
            css: ['<%= project.assets %>/sass/main.scss']
        },
        sass: {
            dev: {
                options: {
                    style: 'expanded',
                    compass: false
                },
                files: {
                    '<%= project.assets %>/css/main.css':'<%= project.css %>'
                }
            }
        },
        watch: {
            sass: {
                files: '<%= project.assets %>/sass/{,*/}*.{scss,sass}',
                tasks: ['sass:dev']
            }
        },
        uglify: {
            target: {
                files: {
                    'app/app.annotated.min.js': ['app.annotated.js']
                }
            }
        },
        ngAnnotate: {
            target: {
                files: {
                    'app/app.annotated.js': ['app/**/*.js']
                }

            }
        }
	});

	grunt.registerTask('default', [
	    'watch',
        'uglify',
        'ngAnnotate'
	]);

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate');
};
