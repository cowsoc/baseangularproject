#!/usr/bin/python
import argparse

#Setting up CLI Parsers
parser    = argparse.ArgumentParser(description="Small little command line utility to create and manage new Angular projects!")
subparser = parser.add_subparsers(title="Available Project Subcommands", dest="command", metavar="------------------------------------------")

start_app_parser = subparser.add_parser("start_app", help="Create a new angular seed project using this template")
start_app_parser.add_argument("app_name", help="New name of the application")

rename_app_parser = subparser.add_parser("rename_app", help="Rename an existing angular application")
rename_app_parser.add_argument("original_app_name", help="Name of your original application")
rename_app_parser.add_argument("new_app_name", help="New name of your application")

parser.parse_args()
print command
