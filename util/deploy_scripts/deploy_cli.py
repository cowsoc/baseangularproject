#!/usr/bin/env python
from boto3.session import Session
import subprocess
import sys
import datetime
import boto3
import json
import uuid
import shutil
import os
import fnmatch

CLOUDFRONT_DISTRIBUTION_ID = 'E3K8MM3LGZFV78'

def main():

    try:
        print "Running command: {}".format(sys.argv[1])
        script_commands[sys.argv[1]](*sys.argv[2::])

    except IndexError as e:
        print 'No command recieved.'
        print 'Usage: setup | upload | list_verions | deploy_version | upload_and_deploy | invalidate_cache | minify'

def setup():
    print "Running setup commands:"
    output = _run_process(["./setup.sh"])

def upload():
    print "Running Upload:"
    current_commit = _run_process(["git", "rev-parse",  "--short", "HEAD"], False).strip()
    current_time = datetime.datetime.now().strftime('%Y.%m.%d_%H.%M.%S')
    _compress_site()

    print "Uploading version -- {}-{}".format(current_time, current_commit)
    print "Uploading Compressed Assets"
    _run_process(["aws", "s3", "cp", "../../temp", "s3://nuvola-labs-io/frontend/{}-{}".format(current_time, current_commit),  
    		"--recursive", "--cache-control", "300",
			"--exclude", "*",
    		"--include", "*.html",
    		"--include", "*.js",
    		"--include", "*.css",
    		"--content-encoding", "gzip",
    		"--cache-control", "s-maxage=300"
    	])

    print
    print "Uploading Image Assets"
    _run_process(["aws", "s3", "cp", "../../temp", "s3://nuvola-labs-io/frontend/{}-{}".format(current_time, current_commit),  
    		"--recursive", "--cache-control", "300",
			"--exclude", "*",
    		"--include", "*.jpg",
    		"--include", "*.png",
    		"--cache-control", "s-maxage=86400"
    	])

    print
    print "Uploading the rest of the site"
    _run_process(["aws", "s3", "cp", "../../temp", "s3://nuvola-labs-io/frontend/{}-{}".format(current_time, current_commit),  
    		"--recursive", "--cache-control", "300",
    		"--exclude", "*.html",
    		"--exclude", "*.js",
    		"--exclude", "*.css",
    		"--exclude", "*.jpg",
    		"--exclude", "*.png",
    	])

    _remove_temp_site()
    return "{}-{}".format(current_time, current_commit)

def list_versions():
    print "List versions in AWS:"
    print
    version_output = _run_process(['/bin/bash', '-c', "aws s3 ls s3://nuvola-labs-io/frontend/ | grep PRE | awk '{ print $2 }' | sed \"s/\// /g\""], False).splitlines()
    production_version = _get_current_production_version()

    for version in version_output:
        if version == production_version:
            print "\033[0;31m{}".format(version)
        else:
            print version

def deploy_version(deployable_version):
    print "Deploying Version: {}".format(deployable_version)
    dist_config = json.loads(_run_process(['aws', 'cloudfront', 'get-distribution', '--id', CLOUDFRONT_DISTRIBUTION_ID], False))
    
    #Cleaning the dist config for redeployment
    etag = dist_config['ETag']
    dist_config = dist_config['Distribution']['DistributionConfig']
    dist_config['Origins']['Items'][0]['OriginPath'] = "/frontend/{}".format(deployable_version)
    dist_config['Origins']['Items'][0]['Id'] = deployable_version
    dist_config['DefaultCacheBehavior']['TargetOriginId'] = deployable_version

    _run_process([
            'aws', 'cloudfront', 'update-distribution', 
            '--id', CLOUDFRONT_DISTRIBUTION_ID,
            '--if-match', etag,
            '--distribution-config', json.dumps(dist_config)
        ], False)

def invalidate_cache():
    print "Invalidating cloudfront cache for current website"

    invalidation_data = {
        'Paths': {
            'Quantity': 1,
            'Items': ["/*"]
        },
        'CallerReference': str(uuid.uuid4())
    }
    _run_process(['aws', 'cloudfront', 'create-invalidation', '--distribution-id', CLOUDFRONT_DISTRIBUTION_ID, '--invalidation-batch', json.dumps(invalidation_data)], False)

def upload_and_deploy():
    version = upload()
    deploy_version(version)
    invalidate_cache()

def annotate_and_uglify():
    _annotate_via_grunt()
    _minify_via_grunt()


def _annotate_via_grunt():
    print "Annotating Javascript using grunt"
    _run_process(['grunt', 'ngAnnotate'], cwd='../../src')

def _minify_via_grunt():
    print "Minifying Annotated Javascript using grunt"
    _run_process(['grunt', 'uglify'], cwd='../../src')


def _run_process(process_arguments, write_stdout=True, cwd=None):
    process = subprocess.Popen(process_arguments, stdout=subprocess.PIPE, cwd=cwd)
    output = ""

    for c in iter(lambda: process.stdout.read(1), ''):
        if write_stdout:
            sys.stdout.write(c)
        output += c

    return output

def _get_aws_session():
    return Session(region_name='us-west-1')

def _get_current_production_version():
    distribution_config = json.loads(_run_process(['aws', 'cloudfront', 'get-distribution', '--id', CLOUDFRONT_DISTRIBUTION_ID], False))
    return distribution_config['Distribution']['DistributionConfig']['Origins']['Items'][0]['Id']

def _compress_site():
    shutil.copytree('../../src/app', '../../temp')

    matches = []
    for root, dirnames, filenames in os.walk('../../temp'):
        for filename in fnmatch.filter(filenames, '*.html'):
            matches.append(os.path.join(root, filename))

        for filename in fnmatch.filter(filenames, '*.css'):
            matches.append(os.path.join(root, filename))

        for filename in fnmatch.filter(filenames, '*.js'):
            matches.append(os.path.join(root, filename))

    for file in matches:
    	print "Gzipping: {}".format(file)
        _run_process(['/usr/bin/gzip', '-9', file])
        _run_process(['/bin/mv', '-f', "{}.gz".format(file) ,file])

def _remove_temp_site():
    shutil.rmtree('../../temp')

def _test():
	pass


script_commands = {
    'setup': setup,
    'upload': upload,
    'list_versions': list_versions,
    'deploy_version': deploy_version,
    'invalidate_cache': invalidate_cache,
    'upload_and_deploy': upload_and_deploy,
    'minify': annotate_and_uglify,
    'test': _test
}

main()
