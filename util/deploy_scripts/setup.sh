#!/bin/bash
virtualenv venv
source venv/bin/activate
pip install -r config/requirements.txt

mkdir -p ~/.aws
cp config/credentials ~/.aws/credentials
cp config/config      ~/.aws/config
