awscli==1.7.44
boto3==1.1.1
botocore==1.1.7
colorama==0.3.3
docutils==0.12
futures==2.2.0
jmespath==0.7.1
pyasn1==0.1.8
python-dateutil==2.4.2
rsa==3.1.4
six==1.9.0
