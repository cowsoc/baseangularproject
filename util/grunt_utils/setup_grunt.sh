#!/bin/bash
npm install -g grunt-cli
npm install -g grunt-init

pushd ../../src/
npm install grunt-contrib-sass --save-dev
npm install grunt-contrib-watch --save-dev
npm install grunt-ng-annotate --save-dev
npm install grunt-contrib-uglify --save-dev
popd
