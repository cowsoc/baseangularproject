#!/bin/bash
arg=$1

if [[ $arg == "start" ]]; then
	echo Starting Grunt
	(cd ../../src && grunt > /dev/null 2>&1) &
elif [[ $arg == "stop" ]]; then
	echo Stopping Grunt...
	ps -ef | grep -v grep | grep grunt | awk '{print $2}' | xargs kill
else
	echo Unrecognized Command
fi
